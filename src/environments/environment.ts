// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    
    /* production */
    // apiBase : 'http://34.71.61.26:8095/',
    // ws_url : 'http://34.123.151.30:8099/',
    // imgApiBase : 'http://34.71.61.26:8095/',
    // googleMapApiKey: 'AIzaSyAR8oOKCw0WxyZPcfmm32pKP-x1jVkez-U',
    // production: false

    /* beta */
    // apiBase : 'http://173.82.95.250:8095/',
    // ws_url : 'http://34.123.151.30:8099/',
    // imgApiBase : 'http://173.82.95.250:8095/',
    // googleMapApiKey: 'AIzaSyAR8oOKCw0WxyZPcfmm32pKP-x1jVkez-U',
    // production: false,
    // googleJsApi:"https://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAR8oOKCw0WxyZPcfmm32pKP-x1jVkez-U",

    /* ghost test */ 
    apiBase : 'http://192.168.8.100:8095/',
    ws_url : 'http://34.123.151.30:8099/',
    imgApiBase : 'http://192.168.8.100:8095/',
    googleMapApiKey: 'AIzaSyAR8oOKCw0WxyZPcfmm32pKP-x1jVkez-U',
    production: false,
    googleJsApi:"https://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAR8oOKCw0WxyZPcfmm32pKP-x1jVkez-U",
};
